#!/bin/bash

if [ ${IP} == '' ]; then
    exit 1
fi
if [ ${PORT} == '' ]; then
    PORT=443
fi
if [ ${USERNAME} == '' ]; then
    USERNAME='admin'
fi
if [ ${PASSWORD} == '' ]; then
    exit 1
fi
if [ ${INTERNALNAME} == '' ]; then
    INTERNALNAME='asa-mgmt'
fi
if [ ${PUBLICNAME} == '' ]; then
    exit 1
fi
if [ ${EMAIL} == '' ]; then
    EMAIL="noreply@${PUBLICNAME}"
fi

if [ ! -f /etc/pki/tls/certs/${INTERNALNAME}.pem ]; then
    echo "${IP} ${INTERNALNAME}" | tee -a /etc/hosts
    :| openssl s_client -showcerts -connect ${INTERNALNAME}:${PORT} -servername ${INTERNALNAME} | openssl x509 | tee -a /etc/pki/tls/certs/${INTERNALNAME}.pem
    c_rehash /etc/pki/tls/certs
    (umask 0077; touch /etc/letsencrypt/asa_creds.txt)
    echo "${INTERNALNAME}:${PORT};${USERNAME};${PASSWORD}" | tee -a /etc/letsencrypt/asa_creds.txt
    (umask 0077; touch /etc/letsencrypt/cli.ini)
    tee -a /etc/letsencrypt/cli.ini <<< "$(cat << EOF
email = ${EMAIL}
text = True
agree-tos = True
debug = True
verbose = True
EOF
)"
    certbot -a certbot-asa:asa -d ${PUBLICNAME} --certbot-asa:asa-host ${INTERNALNAME}:${PORT} --certbot-asa:asa-castore /etc/pki/tls/certs

else

    certbot renew -a certbot-asa:asa -d ${PUBLICNAME} --certbot-asa:asa-host ${INTERNALNAME}:${PORT} --certbot-asa:asa-castore /etc/pki/tls/certs

fi
