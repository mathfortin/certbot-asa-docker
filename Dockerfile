FROM centos:7
MAINTAINER "Mathieu Fortin" <mathieu@bummis.com>

RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

RUN yum -y update;\
yum -y install git openssl-perl; \
sed -i 's/^verify=.*$/verify=enable/' /etc/python/cert-verification.cfg; \
yum -y install epel-release yum-utils gcc openssl-devel python-devel; \
yum -y --enablerepo=epel install python-pip; \
pip install 'requests>=2.9.0'; \
pip install 'certbot'; \
pip install 'pyopenssl>=0.14'; \
pip install 'pyasn1'; \
mkdir -pm 0700 /etc/letsencrypt /var/lib/letsencrypt /var/log/letsencrypt /var/log/httpd

RUN git clone https://bitbucket.org/mathfortin/certbot-asa.git /tmp/certbot-asa; \
(cd /tmp/certbot-asa; python /tmp/certbot-asa/setup.py install)

COPY run.sh /root/run.sh

RUN chmod +x /root/run.sh

EXPOSE 80 443

CMD ["/root/run.sh"]

## Comment the above and uncomment the below if you want to run the run.sh script manually.
#CMD ["/usr/sbin/init"]
