# Docker Image for certbot-asa

##Build:

```
docker build --rm -t certbot-asa/v0.1 .
```

## Run using:

```
docker run --name certbot-asa -e IP='<ASA IP>' -e PORT='<USUALLY 443>' -e USERNAME='<ASA USERNAME>' -e PASSWORD='<ASA PASSWORD>' -e INTERNALNAME='<EXAMPLE asa-mgmt>' -e PUBLICNAME='<PUBLIC DNS NAME>' -e EMAIL='<YOUR EMAIL>' -p 8080:80 -p 8443:443 certbot-asa/v0.1
```

Docker container will start, issue certificate and transfer on your ASA.

## Renewal should be performed by running:

```
docker start certbot-asa/v0.1
```

for verbose output, you can run:

```
docker start -a -i certbot-asa/v0.1
```
